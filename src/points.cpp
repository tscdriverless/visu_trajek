#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayDimension.h>
#include <cmath>

float datayy[40];
float dataxy[40];
float datayb[40];
float dataxb[40];
int dimb;
int dimy;

//Call to get data from the subcribted Topic
void datayyCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimy=vel->layout.dim[0].size;
for (int i =0; i<dimy; i++)
{	
	datayy[i]=vel->data[i];
}
}

//Call to get data from the subcribted Topic
void dataxyCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimy=vel->layout.dim[0].size;
for (int i =0; i<dimy; i++)
{	
	dataxy[i]=vel->data[i];
}
}
//Call to get data from the subcribted Topic
void dataybCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimb=vel->layout.dim[0].size;
for (int i =0; i<dimb; i++)
{	
	datayb[i]=vel->data[i];
}
}

//Call to get data from the subcribted Topic
void dataxbCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimb=vel->layout.dim[0].size;
for (int i =0; i<dimb; i++)
{	
	dataxb[i]=vel->data[i];
}
}

int main( int argc, char** argv )
{
	ros::init(argc, argv, "points");
	ros::NodeHandle n;
	ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);

	ros::Rate r(20);

//create a subscriber
	ros::Subscriber subxb = n.subscribe("veh_data_x_blue", 1000, dataxbCallback);
	ros::Subscriber subyb = n.subscribe("veh_data_y_blue", 1000, dataybCallback);
	ros::Subscriber subxy = n.subscribe("veh_data_x_yellow", 1000, dataxyCallback);
	ros::Subscriber subyy = n.subscribe("veh_data_y_yellow", 1000, datayyCallback);

  while (ros::ok())
  {
	ros::spinOnce();
	visualization_msgs::Marker ypoints, bpoints, opoints;
	ypoints.header.frame_id = bpoints.header.frame_id = opoints.header.frame_id = "/my_frame";
	ypoints.header.stamp = bpoints.header.stamp = opoints.header.stamp = ros::Time::now();
	ypoints.ns = bpoints.ns = opoints.ns = "points";
	ypoints.action = bpoints.action = opoints.action = visualization_msgs::Marker::ADD;
	ypoints.pose.orientation.w = bpoints.pose.orientation.w = opoints.pose.orientation.w = 1.0;

	ypoints.id = 0;
	bpoints.id = 1;
	opoints.id = 2;

	ypoints.type = bpoints.type = opoints.type = visualization_msgs::Marker::POINTS;

	// POINTS markers use x and y scale for width/height respectively
	ypoints.scale.x = bpoints.scale.x = opoints.scale.x = 0.2;
	ypoints.scale.y = bpoints.scale.y = opoints.scale.y = 0.2;

    // blue Points are blue
	bpoints.color.b = 1.0;
	bpoints.color.a = 1.0;

    // yellow Points are yellow
	ypoints.color.r = 0.5;
	ypoints.color.g = 0.5;
	ypoints.color.b = 0.0;
	ypoints.color.a = 1.0;
    
    // orange Points are orange
	opoints.color.r = 1;
	opoints.color.g = 0.5;
	opoints.color.b = 0.0;
	opoints.color.a = 1.0;

    // Create the vertices for the points and lines
	if (dimy!=0)    
	{	
		for (uint32_t i = 0; i < (dimy-datayy[dimy-1]); ++i)
		{
			geometry_msgs::Point p;
			p.y = datayy[i];
			p.x = dataxy[i];
			p.z = 0;
     
			ypoints.points.push_back(p);
		}
    for (uint32_t i = (dimy-datayy[dimy-1]); i < dimy-1; ++i)
    {
      geometry_msgs::Point p;
      p.y = datayy[i];
      p.x = dataxy[i];
      p.z = 0;
     
      opoints.points.push_back(p);
    }
	}
	for (uint32_t i = 0; i < dimb; ++i)
	{
		geometry_msgs::Point p;
		p.y = datayb[i];
		p.x = dataxb[i];
		p.z = 0;

		bpoints.points.push_back(p);
	}

	marker_pub.publish(bpoints);
	marker_pub.publish(ypoints);
	marker_pub.publish(opoints);
	r.sleep();


  }
}


