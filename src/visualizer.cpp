/*
 * Copyright (c) 2017, 2018, Eric Gorkow, Team Starcraft e.V., Ilmenau, Germany
 * All rights reserved.
 */

// includes
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include "geometry_msgs/Twist.h"
#include <tf/transform_broadcaster.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayDimension.h>
#include <cmath>


#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using boost::asio::ip::udp;


float data[13];
float datayt[11];
float dataxt[11];
int dimt;
float lensid=0.6;
float lenfro=0.822;
float stepwidth=0.1;

uint32_t udpd[100];

//Call to get data from the subcribted Topic
void dataCallback(const geometry_msgs::Twist::ConstPtr& vel) {
 //Since vel is a const pointer you cannot edit the values inside but have to use the copy new_vel.
	geometry_msgs::Twist new_vel = *vel; 
	data[0]=new_vel.linear.x; //x Car
	data[1]=new_vel.linear.y; //y Car
	data[4]=cos(new_vel.angular.y); // angle z tire front
	data[5]=cos(new_vel.angular.z); // angle z Car
	data[6]=sin(new_vel.angular.y); // angle w tire front
	data[7]=sin(new_vel.angular.z); // angle w Car
	data[2]=cos((new_vel.angular.z-(M_PI/2))*2)*lensid; //x side rigth
	data[3]=sin((new_vel.angular.z-(M_PI/2))*2)*lenfro; // x front
	data[8]=-sin((new_vel.angular.z-(M_PI/2))*2)*lensid; //y side rigth
	data[9]=cos((new_vel.angular.z-(M_PI/2))*2)*lenfro; //y front
}

//Call to get data from the subcribted Topic
void dataytCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimt=vel->layout.dim[0].size;
for (int i =0; i<dimt; i++)
{	
	datayt[i]=vel->data[i];
}
}

//Call to get data from the subcribted Topic
void dataxtCallback(const std_msgs::Float32MultiArray::ConstPtr& vel) {

dimt=vel->layout.dim[0].size;
for (int i =0; i<dimt; i++)
{	
	dataxt[i]=vel->data[i];
}
}
// own class server udp

class UDPClient
{
public:
    boost::asio::io_service io_service;
    udp::socket socket;
    udp::endpoint receiver_endpoint;


    UDPClient(int);
    void do_receive();
    void handle_receive(const boost::system::error_code& error, size_t);
	void handler(){
		io_service.stop();
	}
};

UDPClient::UDPClient(int Port)
    : io_service(),
      socket(io_service, udp::endpoint(udp::v4(),Port))
{
	do_receive();
	boost::asio::deadline_timer t(io_service, boost::posix_time::microseconds(100));
	t.async_wait(boost::bind(&UDPClient::handler,this));
	io_service.run();
	io_service.reset();

}

void UDPClient::do_receive()
{
    socket.async_receive_from(boost::asio::buffer(udpd,4*100), receiver_endpoint,
                               boost::bind(&UDPClient::handle_receive, this,
                               boost::asio::placeholders::error,
                               boost::asio::placeholders::bytes_transferred));
}

void UDPClient::handle_receive(const boost::system::error_code& error, size_t bytes_transferred)
{	
    //if (!error || error == boost::asio::error::message_size)
        //do_receive();
}



// Main void
int main( int argc, char** argv )
{
	
	// trying to establish raw UPD connection to SL
/*udpd[0]=7;
udpd[1]=1;
	boost::asio::io_service io_service;
	udp::socket socket(io_service);
	udp::endpoint remote_endpoint_r, remote_endpoint_s ;
	remote_endpoint_s = udp::endpoint(boost::asio::ip::address::from_string("192.168.0.74"),5000);
	remote_endpoint_r = udp::endpoint();	
	socket.open(udp::v4());
	boost::system::error_code err;*/

	//initialize ros with visualizer
	ros::init(argc, argv, "visualizer");

	//n as Nodehandler
	ros::NodeHandle n;

	//r as RosRate is the frequency with which we want to loop the publisher.
	//Because we write this as visualization for the Simulink data coming over
	//the subscribed topic, we use a comfortable frequency for our eyes 
	ros::Rate r(20);

	//create a publisher to publisch in the future at the topic visualisation_marker
	//with buffer size 1
	ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 3);

	//create a subscriber
	ros::Subscriber sub = n.subscribe("veh_data", 1000, dataCallback);
	ros::Subscriber subxb = n.subscribe("veh_data_x_traj", 1000, dataxtCallback);
	ros::Subscriber subyb = n.subscribe("veh_data_y_traj", 1000, dataytCallback);

	
	//tf broadcaster
	tf::TransformBroadcaster br;
	tf::Transform transform;

	// Set our move shape type to be a mesh reaurce, sqaure and line-list
	uint32_t shape = visualization_msgs::Marker::MESH_RESOURCE;
	uint32_t tshape = visualization_msgs::Marker::CUBE;
	uint32_t llshape = visualization_msgs::Marker::LINE_STRIP;

	while (ros::ok())
	{	
		
		//UDPClient udpclient(9001);
		//socket.send_to(boost::asio::buffer(udpd,4*100),remote_endpoint_s, 0, err);
	
		
//runs all Callbacks once
		ros::spinOnce();

		//allocate Markers
		visualization_msgs::Marker marker;
		visualization_msgs::Marker tiref;
		visualization_msgs::Marker tirer;
		visualization_msgs::Marker traj;

		// Set the frame ID and timestamp.  See the TF tutorials for information on these.
		marker.header.frame_id = tiref.header.frame_id = tirer.header.frame_id =  traj.header.frame_id ="/my_frame";
		marker.header.stamp = tiref.header.stamp = tirer.header.stamp = traj.header.stamp = ros::Time::now();

		// Set the namespace and id for this marker.  This serves to create a unique ID
		// Any marker sent with the same namespace and id will overwrite the old one
		marker.ns = "visualization_mark";
		marker.id = 2;

		tiref.ns = "visualization_tiref";
		tiref.id = 3;

		tirer.ns = "visualization_tirer";
		tirer.id = 4;

		traj.ns = "visualization_traj";
		traj.id = 5;

		// Set the marker type.  Initially this is CUBE
		marker.type = shape;
		marker.mesh_resource="package://visu_trajek/car.stl";
		tiref.type = tirer.type = tshape;
		traj.type=llshape;

		// Set the marker action.  Options are ADD, DELETE and DELETEALL (3)
    		marker.action = visualization_msgs::Marker::ADD;
		tiref.action = visualization_msgs::Marker::ADD;
		tirer.action = visualization_msgs::Marker::ADD;
		traj.action = visualization_msgs::Marker::ADD;

		// Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
		marker.pose.position.x = data[0];
		marker.pose.position.y = data[1];
		marker.pose.position.z = 0;
		marker.pose.orientation.x = tiref.pose.orientation.x = tirer.pose.orientation.x = 0;
		marker.pose.orientation.y = tiref.pose.orientation.y = tirer.pose.orientation.y = 0; 
		marker.pose.orientation.z = data[5];
		marker.pose.orientation.w = data[7];
		tiref.pose.orientation.z = tirer.pose.orientation.z = data[4];
		tiref.pose.orientation.w = tirer.pose.orientation.w = data[6];

		// geometry_msgs::Point p;
		// Create the points for the fronttires
		tiref.pose.position.y = data[1]+data[8]+data[9];
		tiref.pose.position.x = data[0]+data[2]+data[3];
		tiref.pose.position.z = 0.15;

		tirer.pose.position.y = data[1]-data[8]+data[9];
		tirer.pose.position.x = data[0]-data[2]+data[3];
		tirer.pose.position.z = 0.15;

		// create the points for the trajectory
		traj.scale.x = 0.1;
		traj.color.r = 1.0;
		traj.color.a= 1.0;
		traj.pose.orientation.w = 1.0;
		
		for (uint32_t i = 0; i<dimt; i++)
		{			
			geometry_msgs::Point p;
			p.z = 0;
			p.y = datayt[i]+data[1];
			p.x = dataxt[i]+data[0];
			traj.points.push_back(p);
		}
		//ROS_INFO("%i",dimt);

		// Set the scale of the marker -- 1x1x1 here means 1m on a side
		marker.scale.x = 0.1;
		marker.scale.y = 0.1;
		marker.scale.z = 0.1;
		
		tiref.scale.x = tirer.scale.x = 0.15;
		tiref.scale.y = tirer.scale.y = 0.3;
		tiref.scale.z = tirer.scale.z = 0.3;

		// Set the color -- be sure to set alpha to something non-zero!

		marker.color.r = 0.0f;
		marker.color.g = 1.0f;
		marker.color.b = 0.0f;
		marker.color.a = 1.0;

		tiref.color.r = tirer.color.r = 1.0f;
		tiref.color.g = tirer.color.g = 0.0f;
		tiref.color.b = tirer.color.b = 0.0f;
		tiref.color.a = tirer.color.a = 1.0;

		marker.lifetime = tiref.lifetime = tirer.lifetime = ros::Duration();

		// Publish the marker
		while (marker_pub.getNumSubscribers() < 1)
		{
			if (!ros::ok())
			{
				return 0;
			}
			ROS_WARN_ONCE("Please create a subscriber to the marker");
			sleep(1);
		}
	//setup broadcaster 
	transform.setOrigin( tf::Vector3(data[0], data[1], 0) );
	transform.setRotation( tf::Quaternion(0, 0, data[5], data[7]) );
	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "my_frame", "moving"));

    	marker_pub.publish(marker);
    	marker_pub.publish(tirer);
    	marker_pub.publish(tiref);
	marker_pub.publish(traj);

	r.sleep();
	}

//socket.close();

}


