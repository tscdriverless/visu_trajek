#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayDimension.h>
#include <cmath>

float datayy[40];
float dataxy[40];
float datayb[40];
float dataxb[40];
float datayo[40];
float dataxo[40];
int dimb=0;
int dimy=0;
int dimo=0;
bool cone_bool = false;
std_msgs::Float32MultiArray::Ptr cone_cb(new std_msgs::Float32MultiArray);

void cone_depth_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
	cone_cb->data = raw_msg->data;
	cone_bool = true;
	//ROS_INFO("%i",(int)raw_msg->data[0]);

}

int main( int argc, char** argv )
{
	ros::init(argc, argv, "points");
	ros::NodeHandle n;
	ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);
	ros::Subscriber sub_cone_pos = n.subscribe<std_msgs::Float32MultiArray>("/combined_cones",1, cone_depth_cb);
	ros::Rate r(20);

//create a subscriber

  while (ros::ok())
  {
	ros::spinOnce();
	dimb=0;
	dimy=0;
	dimo=0;
	if (cone_bool)
	{
		for(int i=1 ; i<=(int)cone_cb->data[0] ; i++)
		{
			float y=cone_cb->data[i*4-3];
			float x=cone_cb->data[i*4-2];
			if (cone_cb->data[i*4-1]==0)
			{
				dimb++;
				dataxb[dimb-1]=x;
				datayb[dimb-1]=y;
			}else{
				if(cone_cb->data[i*4-1]==1)
				{
					dimy++;
					dataxy[dimy-1]=x;
					datayy[dimy-1]=y;
				}else{
					if(cone_cb->data[i*4-1]==4)
					{
						if (x>1 && x<7 && y<2.5 && y>-2.5)
						{
							dimo++;
							dataxo[dimo-1]=x;
							datayo[dimo-1]=y;
						}
					}else{
						dimo++;
						dataxo[dimo-1]=x;
						datayo[dimo-1]=y;							
					}
				}
			}
		}
	

	visualization_msgs::Marker ypoints, bpoints, opoints;
	ypoints.header.frame_id = bpoints.header.frame_id = opoints.header.frame_id = "/my_frame";
	ypoints.header.stamp = bpoints.header.stamp = opoints.header.stamp = ros::Time::now();
	ypoints.ns = bpoints.ns = opoints.ns = "points";
	ypoints.action = bpoints.action = opoints.action = visualization_msgs::Marker::ADD;
	ypoints.pose.orientation.w = bpoints.pose.orientation.w = opoints.pose.orientation.w = 1.0;

	ypoints.id = 0;
	bpoints.id = 1;
	opoints.id = 2;

	ypoints.type = bpoints.type = opoints.type = visualization_msgs::Marker::POINTS;

	// POINTS markers use x and y scale for width/height respectively
	ypoints.scale.x = bpoints.scale.x = opoints.scale.x = 0.2;
	ypoints.scale.y = bpoints.scale.y = opoints.scale.y = 0.2;

    // blue Points are blue
	bpoints.color.b = 1.0;
	bpoints.color.a = 1.0;

    // yellow Points are yellow
	ypoints.color.r = 0.5;
	ypoints.color.g = 0.5;
	ypoints.color.b = 0.0;
	ypoints.color.a = 1.0;
    
    // orange Points are orange
	opoints.color.r = 1;
	opoints.color.g = 0.5;
	opoints.color.b = 0.0;
	opoints.color.a = 1.0;

    // Create the vertices for the points and lines
		if (dimy!=0)    
		{	
			for (uint32_t i = 0; i < dimy; ++i)
			{
				geometry_msgs::Point p;
				p.y = datayy[i];
				p.x = dataxy[i];
				p.z = 0;
	     
				ypoints.points.push_back(p);
			}
	    for (uint32_t i = 0; i < dimo; ++i)
	    {
		geometry_msgs::Point p;
		p.y = datayo[i];
		p.x = dataxo[i];
		p.z = 0;
	     
		opoints.points.push_back(p);
	    }
		}
		for (uint32_t i = 0; i < dimb; ++i)
		{
			geometry_msgs::Point p;
			p.y = datayb[i];
			p.x = dataxb[i];
			p.z = 0;

			bpoints.points.push_back(p);
		}

		marker_pub.publish(bpoints);
		marker_pub.publish(ypoints);
		marker_pub.publish(opoints);
}
		r.sleep();


  }
}


